'''
Created on 21 juil. 2018

@author: cyr0p
'''

from __future__ import print_function
from dbgunicorn import *
from capstone import *





def hook_code(uc, address, size, user_data):
    print(">>> Tracing instruction at 0x%x, instruction size = 0x%x" %(address, size))
    


def main():
    # code to be emulated
    X86_CODE32 = "\x6a\x31\x58\x99\xcd\x80\x89\xc3\x89\xc1\x6a\x46\x58\xcd\x80\xb0\x0b\x52\x68\x6e\x2f\x73\x68\x68\x2f\x2f\x62\x69\x89\xe3\x89\xd1\xcd\x80" # INC ecx; DEC edx
    ADDRESS = 0x1000000

    STACK_ADDRESS= 0x8000

    md = Cs(CS_ARCH_X86, CS_MODE_32)
    for i in md.disasm(X86_CODE32, ADDRESS):
        print("0x%x:\t%s\t%s" %(i.address, i.mnemonic, i.op_str))


    # memory address where emulation starts

    print("Emulate i386 code")
    try:
        # Initialize emulator in X86-32bit mode
        mu = DbgUnicorn(UC_ARCH_X86, UC_MODE_32)

        # map 2MB memory for this emulation
        mu.mem_map(ADDRESS, 2 * 1024 * 1024)

        # map stack
        mu.mem_map(STACK_ADDRESS, 1024 * 1024)

        # write machine code to be emulated to memory
        mu.mem_write(ADDRESS, X86_CODE32)

        # initialize machine registers
        mu.reg_write(UC_X86_REG_ECX, 0x1234)
        mu.reg_write(UC_X86_REG_EDX, 0x7890)
        mu.reg_write(UC_X86_REG_ESP, STACK_ADDRESS+1024*1024)
    
        # tracing all instructions with customized callback
        mu.hook_add(UC_HOOK_CODE, hook_code)

        # emulate code in infinite time & unlimited instructions
        mu.emu_start(ADDRESS, ADDRESS + len(X86_CODE32))
        #mu.cmdloop(">")
        # now print out some registers
        print("Emulation done. Below is the CPU context")

 
    except UcError as e:
        print("ERROR: %s" % e)



if __name__ == '__main__':
    main()
