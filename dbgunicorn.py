'''
Created on 21 juil. 2018

@author: cyr0p
'''
from __future__ import print_function
import cmd
from unicorn import *
from unicorn.x86_const import *
from capstone import *
from psutil._common import addr
import binascii

class DbgUnicorn(Uc,cmd.Cmd):
    '''
    classdocs
    '''
    start =0x0
    curAddr=0x0
    end=0xFFFffFFF
    firstInst=False#to handle breakpoint : we want to execute the instruction afterwards !
    bp_list=[]
    step=False
    def __init__(self, arch, mode):
        '''
        Constructor
        '''
        Uc.__init__(self,arch,mode)
        self.hook_add(UC_HOOK_CODE, self.hook_code)
        cmd.Cmd.__init__(self)
        
    
    def do_go(self,line):
        Uc.emu_start(self, self.curAddr, self.end)
    def do_step(self,line):
        self.step=True
        Uc.emu_start(self, self.curAddr, self.end)
    
    def do_add_bp(self,line):
        line=line.replace("\n","")
        self.bp_list.append(int(line,16))
    
    def do_list_bp(self,line):
        for i in self.bp_list:
            print("%x\n"% i)
    
    def do_info_registers(self,line):
        #TODO : one for every arch
        r_eax = self.reg_read(UC_X86_REG_EAX)
        r_ebx = self.reg_read(UC_X86_REG_EBX)
        r_ecx = self.reg_read(UC_X86_REG_ECX)
        r_edx = self.reg_read(UC_X86_REG_EDX)
        r_eip = self.reg_read(UC_X86_REG_EIP)
        r_esp = self.reg_read(UC_X86_REG_ESP)
        r_ebp = self.reg_read(UC_X86_REG_EBP)
        print(">>> EIP = 0x%x" %r_eip)
        print(">>> ESP = 0x%x" %r_esp)
        print(">>> EBP = 0x%x" %r_ebp)
        print(">>> EAX = 0x%x" %r_eax)
        print(">>> EBX = 0x%x" %r_ebx)
        print(">>> ECX = 0x%x" %r_ecx)
        print(">>> EDX = 0x%x" %r_edx)
    
    def do_hexdump(self, line):
        startAddr=int(line.split(" ")[0],0)
        size=int(line.split(" ")[1],0)
        data=self.mem_read(startAddr, size)
        print(binascii.hexlify(data))
        pass
    
    
    def hook_code(self,uc, address, size, user_data):
        if self.firstInst == False:
            if (address in self.bp_list) or (self.step==True):
                self.step=False
                Uc.emu_stop(self)
                self.curAddr=address
                self.firstInst=True
            
        else :
            self.firstInst=False
    

    
    
    
    def do_EOF(self, line):
        return True
    
    def emu_start(self, start, end):
        self.start=start
        self.curAddr=start
        self.end=end
        self.cmdloop()
        